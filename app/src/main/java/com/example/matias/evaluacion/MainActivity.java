package com.example.matias.evaluacion;

import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity
    implements Personales.OnFragmentInteractionListener, Contacto.OnFragmentInteractionListener, Sesion.OnFragmentInteractionListener
{

    Personales personales;
    Contacto contacto;
    Sesion sesion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        personales=new Personales();
        getSupportFragmentManager().beginTransaction().add(R.id.contenedor, personales).commit();

    }

    @Override
    public void onFragmentInteraction(String nFragment, String nEvento) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        personales = new Personales();
        contacto = new Contacto();
        sesion = new Sesion();

        if(nFragment.equals("PersonalesFragment")){
            if(nEvento.equals("BUTTON_PRUEBA_CLICK")){

                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

                transaction.replace(R.id.contenedor, contacto);

                transaction.commit();
            }
        }

        if(nFragment.equals("ContactoFragment")){
            if(nEvento.equals("BUTTON_PRUEBA_CLICK")){



                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

                transaction.replace(R.id.contenedor, sesion);


                transaction.commit();
            }
        }

        if(nFragment.equals("SesionFragment")){
            if(nEvento.equals("BUTTON_PRUEBA_CLICK")){



                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

                transaction.replace(R.id.contenedor, personales);


                transaction.commit();
            }
        }

    }


}
